import { Component, OnInit, ElementRef } from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})

export class TimerComponent implements OnInit {

  seconds: number;
  minutes: number;
  intervalId : number;

  secondsLap: number = 0;
  minutesLap: number = 0;
  OldMinutes: number = 0;
  oldSecond: number = 0;
  cycle: number = 0;
  cycleLapHtml: string;

  constructor(private elementRef: ElementRef) {}

  ngOnInit() {
      this.seconds = 0;
      this.minutes = 0;
  }

  startTime() {
      this.intervalId = setInterval(() => {
          this.seconds++;
          if(this.seconds > 19) {
              this.minutes++;
              this.seconds = 0;
          }
      }, 1000);
  }

  stopTime() {
      this.seconds = this.seconds;
      this.minutes = this.minutes;
      clearInterval(this.intervalId);
      this.intervalId = -1;
  }

  saveLap() {
      this.secondsLap = this.seconds - this.oldSecond;
      this.oldSecond = this.seconds;

      this.oldMinutes = this.minutes;
      this.minutesLap = this.oldMinutes;

      if(this.minutesLap > 0) {
          this.secondsLap = this.secondsLap + (20*this.oldMinutes);
          this.minutesLap = this.minutes - this.oldMinutes;
      }
      alert(this.minutesLap);

      this.cycle++;

      let cycle_number_div = this.elementRef.nativeElement.querySelector('#cycle_number');
      cycle_number_div.insertAdjacentHTML('beforeend', `<span style="color:blue">Lap(${this.cycle})</span> => ${this.minutesLap} : ${this.secondsLap} <br/>`);

      // this.domService.appendComponentToBody(`<span style="color:blue">Lap(${this.cycle})</span> => ${this.minutesLap} : ${this.secondsLap}`);
      // this.cycleLapHtml = `<span style="color:blue">Lap(${this.cycle})</span> => ${this.minutesLap} : ${this.secondsLap}`
      // this.cycleLapHtml.nativeElement.innerHTML = `<span style="color:blue">Lap(${this.cycle})</span> => ${this.minutesLap} : ${this.secondsLap}`;
  }

}
