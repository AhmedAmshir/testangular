import { Injectable } from '@angular/core';
import { Dummy } from '../dummy';
import { Dummies } from '../dummy-data';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Injectable()
export class DummyService {

  constructor() { }

  getDummies(): Observable<Dummy[]> {
      return of(Dummies);
  }

  getDummy(id:number): Observable<Dummy>  {
      return of(Dummies.find((hero) => hero.id === id));
  }

}
