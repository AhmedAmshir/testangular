import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Video } from '../videos/videos.component';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class VideosService {

   apiKey:string;
   projectId:string;
   channelId:string;
   maxResults:number;
   private _url:string;

   constructor(public http:HttpClient) {
      this.apiKey = 'AIzaSyALzUw5mqzpkm7D1Ty5qfpXOEOGFRNKkGQ';
      this.projectId = '7lCDEYXw3mM';
      this.channelId = 'UCmYew1VwdbpHnSFG-CQ1A-A';
      this.maxResults = 12;

      this._url = 'https://www.googleapis.com/youtube/v3/search?id=' + this.projectId + '&key=' + this.apiKey + '&part=snippet&type=snippet,id&channelId=' + this.channelId + '&maxResults=' + this.maxResults
   }

   getVideos(): Observable<Video[]> {
      return this.http.get<Video[]>(this._url)
      .catch(this.errorHandler);
    //   .map((videos) => videos.items);
   }

   errorHandler(error: HttpErrorResponse) {
       return Observable.throw(error.message || "Server Error");
   }
}
