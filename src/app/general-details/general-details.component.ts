import { Component, OnInit, Input, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DummyService } from '../services/dummy.service';
import { Dummy } from '../dummy';
import { Location } from '@angular/common';

@Component({
  selector: 'app-general-details',
  templateUrl: './general-details.component.html',
  styleUrls: ['./general-details.component.css']
})
export class GeneralDetailsComponent implements OnInit {

  @Input() dummy:Dummy;
  color:string;
  
  
  constructor(private router:Router, private route:ActivatedRoute, private dummyService:DummyService, private location:Location) { }
  
  ngOnInit() {
      this.getDummy();
  }

  getDummy() {
      let id = +this.route.snapshot.paramMap.get('id');
      this.dummyService.getDummy(id).subscribe(dummy => this.dummy = dummy);
  }

  goBack(): void {
      this.router.navigate(['/general']);
  }

}
