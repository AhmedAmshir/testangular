import { Component, OnInit, Input } from '@angular/core';
import { Video } from '../videos/videos.component';
import { SafeResourceUrl, SafeUrl, DomSanitizer } from "@angular/platform-browser";
// import { dateDifferencePip } from '../app.component';

@Component({
  selector: 'app-video-details',
  templateUrl: './video-details.component.html',
  styleUrls: ['./video-details.component.css']
})


export class VideoDetailsComponent implements OnInit {

  @Input() video:Video;

  url:string;
  differ:string;
  videoURL:SafeResourceUrl;

  constructor(private domSanitizer:DomSanitizer) { }

  divStyle = "center";
  dateStyle = { 'text-decoration':'italic', 'color':'blue', 'font-family':'bold', 'font-size':'16px' };

  Today = new Date();

  ngOnInit() {
  }

  getTrustVideoUrl(id) {
      this.url = 'https://www.youtube.com/embed/' + id;
      return this.videoURL = this.domSanitizer.bypassSecurityTrustResourceUrl(this.url); 
  }

}
