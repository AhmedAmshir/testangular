import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[coloredHeader]'
})
export class ColoredHeaderDirective {

  color:string;

  constructor(public element: ElementRef) {
      // element.nativeElement.style.color = "blue";
  }

  @HostListener('mouseenter') changeColor() {
      this.element.nativeElement.style.color = 'hotpink';
  }
  @HostListener('mouseleave') resetColor() {
      this.element.nativeElement.style.color = 'black';
  }

}
