import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { VideosComponent } from './videos/videos.component';
import { VideosService } from './services/videos.service';
import { VideoDetailsComponent } from './video-details/video-details.component';
import { dateDifferencePip } from './app.component';
import { DomSanitizer } from '@angular/platform-browser';
import { GeneralComponent } from './general/general.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { AppRoutingModule } from './/app-routing.module';
import { DummyService } from './services/dummy.service';
import { GeneralDetailsComponent } from './general-details/general-details.component';
import { ColoredHeaderDirective } from './colored-header.directive';
import { TimerComponent } from './timer/timer.component';
import { DomService } from './services/dom.service';

@NgModule({
  declarations: [
    AppComponent,
    VideosComponent,
    VideoDetailsComponent,
    dateDifferencePip,
    GeneralComponent,
    NotfoundComponent,
    GeneralDetailsComponent,
    ColoredHeaderDirective,
    TimerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [VideosService, DummyService, DomService],
  bootstrap: [AppComponent]
})
export class AppModule { }
