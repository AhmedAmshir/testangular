import { Component, Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  name = '';

  ulStyle = 'none';

}

@Pipe({ name: 'dateDifference' })

export class dateDifferencePip {
    transform(differ, date1, date2) {
        let differnce;
        if(differ) {
            return differnce = differ;
        } else {
            date1 = Date.parse(date1);
            date2 = Date.parse(date2);

            differnce = Math.round((date1-date2)/(1000*60*60*24));
            return differnce;
        }
    }
}
