import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { VideosComponent } from './videos/videos.component';
import { GeneralComponent } from './general/general.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { TimerComponent } from './timer/timer.component';
import { GeneralDetailsComponent } from './general-details/general-details.component';


const appRoutes: Routes = [
    {
        path: '', redirectTo: 'general', pathMatch: 'full'
    },
    {
        path:'videos', component: VideosComponent
    },
    {
        path:'general', component: GeneralComponent
    },
    {
        path:'general/:id', component: GeneralDetailsComponent
    },
    {
        path:'stop-watch', component: TimerComponent
    },
    {
      path: '**', component: NotfoundComponent
    }
]


@NgModule({

  imports: [ RouterModule.forRoot(appRoutes) ],
  exports: [ RouterModule ],

})
export class AppRoutingModule { }
