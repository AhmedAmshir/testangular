import { Component, OnInit } from '@angular/core';
import { VideosService } from '../services/videos.service';


@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css']
})
export class VideosComponent implements OnInit {

    videos: Video[];
    channelName:string;
    selectedVideo: Video;
    public errorMsg;

    constructor(private videosService:VideosService) {}

    ngOnInit() {
        this.videosService.getVideos().subscribe((videos) => {
          this.videos = videos.items,
          this.channelName = videos.items.map((video) => video.snippet.channelTitle).slice(0,1),
          error => this.errorMsg = error
        });
    }

    onSelectVideo(video:Video) {

        this.selectedVideo = video;
    }

}

export interface Video {
    kind:string,
    etag:string,
    nextPageToken:string,
    regionCode:string,
    pageInfo: {
        totalResults:number,
        resultsPerPage:number
    },
    items: [
        {
        etag:string,
        id:{
        kind:string,
        videoId:string
        },
        snippet: {
            publishedAt:string,
            channelId:string,
            thumbnails: {
                default: {
                    url:string,
                    width:number,
                    height:number
                }
            }
        },
        channelTitle: string
      }
   ]
}
