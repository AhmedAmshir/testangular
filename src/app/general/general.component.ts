import { Component, OnInit } from '@angular/core';
import { DummyService } from '../services/dummy.service';
import { Dummy } from '../dummy';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css']
})
export class GeneralComponent implements OnInit {

  dummyData: Dummy[];

  constructor(private dummyService:DummyService) { }

  ngOnInit() {

    this.getDummiesData();
    
  }

  getDummiesData() {
      this.dummyService.getDummies().subscribe(dummies => this.dummyData = dummies);
  }

}
